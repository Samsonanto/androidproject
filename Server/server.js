var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var passport = require('passport');
var mongoose = require('mongoose');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var User = require('./model/user');
var ejs = require('ejs'); 
var bodyParser = require('body-parser');  
var morgan = require("morgan");



var app = express();

// Logging using morgan
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

//Passport
require("./.config/passport.js")(passport);
app.use(passport.initialize());


// Routes

var api = express.Router();
require("./routes/api.js")(passport,api);
app.use("/api",api);


var auth = express.Router();
require("./routes/login")(passport,auth);
app.use(auth);



// MongoDB Connection

mongoose.connect("mongodb://localhost/testdb",(err)=>{
  
console.log("err :"+err);

});
// mongoose.connect(process.env.DATABASEURL);



  //  require('./seedDB.js')(User);



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;


