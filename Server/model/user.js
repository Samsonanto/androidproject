var mongoose = require("mongoose");
var bcrypt = require("bcrypt-nodejs");


var userSchema = new mongoose.Schema({
	name: String,
	password: String,
	email: String,
	phone: Number,
	history:[{
		qrCode:String,
		datePrc:Date,
		time:String,
		src:String,
		dst:String,
		cst:Number,
		qty:Number,
		typ:String
	}] 
});


userSchema.methods.generateHash = function (password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
	return bcrypt.compareSync(password, this.password);
};



module.exports = mongoose.model("User", userSchema);