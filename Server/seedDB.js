var faker = require("faker");
var uuid = require("uuidjs");
var fs = require("fs");

module.exports = function(User){
    
    for(var j=0;j<50;j++)
    {

    
        var station = ["CST",
                        "Churchgate",
                        "Bandra",
                        "Vashi",
                        "Andari",
                        "Wadala",
                        "CBD"]

        var u = new User();

        var phone = faker.phone.phoneNumberFormat();
        u.phone =  phone.slice(0,3) + phone.slice(4,7)+phone.slice(8,12);
        u.name = faker.name.firstName();
        u.email = faker.internet.email();
        u.password = u.generateHash("12345");

        for(i =0;i<10;i++)
        {
            var history = {
                qrCode:String,
                datePrc:Date,
                time:String,    
                src:String,
                dst:String,
                qty:Number,
                cst:Number,
                typ:String
            };
            history["qrCode"] = uuid.generate();
            var date = faker.date.recent();
            history["datePrc"] = date;
            history["time"] = date.toString().slice(12,19);
            var src = faker.random.number(6);
            do{
                var dst = faker.random.number(6);
            }while(dst == src);

            history["src"] = station[src];
            history["dst"] = station[dst];
            history["qty"] = faker.random.number(30);
            if(faker.random.boolean())
                history["typ"] = "Return";
            else    
                history["typ"]= "Single";
            if(history.typ === "Return")
                history["cst"] = 10 * parseInt(history.qty);
            else
                history["cst"] = 5 * parseInt(history.qty);

            u.history.push(history);
        }
        fs.appendFile("phone.txt",phone+"\n",(err)=>{
            if(err) console.log( err);
        });
        u.save();
    }

};


function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}