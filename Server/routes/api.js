var uuid = require("uuidjs");
var User = require("../model/user");
var moment =require("moment-timezone");
var time = moment();
var isodate = require("isodate");

module.exports = function(passport,router){
    router.use(passport.authenticate('bearer',{session : false})); 


    router.post("/bookTicket",(req,res)=>{
        console.log(time.tz("Asia/Kolkata").format());
        history = {};
        history.qrCode = uuid.generate();
        history.datePrc = time.tz("Asia/Kolkata")._d;
        history.time = history.datePrc.toString().slice(11,19);
        history.src = req.body.src;
        history.dst = req.body.dst;
        history.cst = parseInt(req.body.cst);
        history.qty = parseInt(req.body.amt);
        history.typ = req.body.type;
        User.findById(req.query.access_token,(err,user)=>{
            if(err)
                return res.json({history : "" , msg : "Server Error"});
            
            user.history.push(history);
            user.save((err)=>{
                if(err){
                    console.log(err)
                    return res.json({history : "",msg:"Unable to update user"});
                }
                return res.json({history : history ,msg:""});
            });

        });
    });
}