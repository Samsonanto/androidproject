module.exports = function(passport,router){


    router.get("/",function(req,res){
        res.render("login" , {signup : "none",login:"block",title : "Login"});
    
    });

router.post('/signup', (req,res,next)=>{
    passport.authenticate('local-signup',(err,user,info)=>{
            if (err) { return res.json({'user' : null , 'msg' : info });}
            if (!user) { return res.json({'user' : null , 'msg' : info }); }
            return res.json({'user' : user , 'msg' : info });
          })(req, res, next);    
    }); 

router.post('/login', (req,res,next)=>{
    console.log(req.body);
    passport.authenticate('local-login',(err,user,info)=>{
        console.log("After Auth");
        if (err) { 
            console.log(err);
            return res.json({'user' : null , 'msg' : info }); }
        if (!user) { return res.json({'user' : null , 'msg' : info }); }
        return res.json({'user' : user , 'msg' : info });
      })(req, res, next);


});

router.get("/logout",function(req,res){
	req.logout();
	res.redirect("/");
});






}
