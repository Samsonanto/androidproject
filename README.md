# Local Railway Booking System Using QR


## Getting Started

This project deals with the development and implementation of smart phone application which is more effective and simple than current ticketing system. The “Local Railway Ticket Booking System using QR Code” can be bought easily anytime, anywhere and the ticket will be present in the customer’s phone in the form of “Quick Response (QR) Code”. 
The user will have to register themselves with the app using there phone no. or email and other details. After successfully creating an account, customer can book a ticket by specifying the source and the destination and book a ticket. The application will generate a QR code of booked ticket which will be used at railway station to scan the ticket QR code. The information for each user is stored in a database for security purpose which is unavailable in the current suburban railway system. Also the ticket checker is provided with an application to search for the user’s ticket with the ticket number in the cloud database for checking purposes.


### Prerequisites


- [Android Studio](https://developer.android.com/studio/install)

- [NodeJS](https://nodejs.org/en/download/)

- [MongoDB](https://docs.mongodb.com/manual/administration/install-community/)
- [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)
- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)


### Installing

The steps are:
 1. [Installing App](#step-1---installing-app)
 1. [Setting Up Server](#step-2---setting-up-server)
 1. [Setting MongoDB Server](#step-3---setting-mongodb-server)

### Step 1 - Installing App

- Extract project .zip file
- Open android studio
- File > Open > /path/to/androidProject
- Click on Run 'app' and then select the device on which the app is to be installed.

### Step 2 - Setting Up Server

I have used [heroku](https://www.heroku.com/) web services to deploy my Server.

- Creating a new app on heroku by clicking on 
    - New > Create new app 
    - Name the app ticket-otg

- Intialize Git on the Server Folder


```
$ cd /path/to/androidProject/Server
$ git init
$ git add .
$ git commit 
```

- Login to heroku with your username and password

```
$ heroku login
```
- Push the locally commited files to heroku 

```
$ git push heroku master
```

- Setting Environment Variables 
    - ticket-otg > Setting > Config Variables 
    - Add (Key,value) pair  DATABASEURL,mongodb://"dbuser":"dbpassword"@"URI"/"dbname" 


Note : mongodb URL will be given to you when you create a database in mLab.

### Step 3 - Setting MongoDB Server 
I have used [mLab](https://mlab.com) for my mongoDB server.

- Creating a db 
    - Create new > sandbox
    - Select a region and give a name to the db "dbname" 

- Creating a user of db
    - "dbname" > Users > add database user


## Deployment

Server is running as soon as git push command is excuted. 
After completion of above 3 steps use the app installed on your android device. 



## Versioning

We use [BitBucket](https://bitbucket.org/) for versioning. Along with [GitKraken](https://www.gitkraken.com) for gui interface

## Authors

* **Samson Anto**


## Acknowledgments

* [Nevon Projects](http://nevonprojects.com/project-ideas/android-project-ideas/)

