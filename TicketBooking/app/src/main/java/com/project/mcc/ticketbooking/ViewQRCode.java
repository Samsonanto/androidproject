package com.project.mcc.ticketbooking;

import android.app.DialogFragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

public class ViewQRCode  extends DialogFragment implements View.OnClickListener{
    View  qrView;
    Button close;
    ImageView qrImg;
    private Bitmap bitmap;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        getDialog().setCanceledOnTouchOutside(false);
        String s = getArguments().getString("qrCode");
        qrView = inflater.inflate(R.layout.qr_code,container,false);
        close = (Button) qrView.findViewById(R.id.close);
        qrImg = (ImageView)qrView.findViewById(R.id.qrCode);
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(s, BarcodeFormat.QR_CODE,200,200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            bitmap = barcodeEncoder.createBitmap(bitMatrix);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        qrImg.setImageBitmap(bitmap);
        close.setOnClickListener(this);
        return qrView;
    }

    @Override
    public void onClick(View v) {

            this.dismiss();
    }
}
