package com.project.mcc.ticketbooking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;


public class Login extends AppCompatActivity {

    EditText phoneNo;
    EditText password;
    Button login;

    Button signup;
    EditText phoneNoS;
    EditText name;
    EditText email;
    EditText passwordS;

    Button login_btu;
    Button signup_btu;

    final String black = "#222222";
    final  String white = "#ffffff";

    View login_page;
    View signup_page;

    ProgressDialog progressDialog;

    private boolean toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressDialog = new ProgressDialog(Login.this);

        phoneNo = (EditText)findViewById(R.id.phoneNo);
        password = (EditText)findViewById(R.id.passwd);
        login = (Button)findViewById(R.id.login);

        login_btu = (Button)findViewById(R.id.login_btu);
        signup_btu = (Button)findViewById(R.id.signup_btu);

        login_page = (View) findViewById(R.id.login_page);
        signup_page = (View) findViewById(R.id.signup_layout);

        signup = (Button) findViewById(R.id.signup);
        passwordS = (EditText) findViewById(R.id.passwdS);
        phoneNoS = (EditText) findViewById(R.id.phoneNoS);
        email = (EditText)findViewById(R.id.email);
        name = (EditText)findViewById(R.id.name);

        myDefault();


        login_btu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!toggle)
                {
                    toggle=!toggle;
                    login_page.setVisibility(View.VISIBLE);
                    signup_page.setVisibility(View.INVISIBLE);

                    login_btu.setBackgroundResource(R.drawable.button_login_focus);
                    login_btu.setTextColor(Color.parseColor(white));
                    signup_btu.setBackgroundResource(R.drawable.button_login_unfocus);
                    signup_btu.setTextColor(Color.parseColor(black));
                    phoneNo.setError(null);
                    password.setError(null);

                }
            }
        });
        signup_btu.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(toggle)
                {
                    toggle=!toggle;
                    login_page.setVisibility(View.INVISIBLE);
                    signup_page.setVisibility(View.VISIBLE);

                    login_btu.setBackgroundResource(R.drawable.button_login_unfocus);
                    login_btu.setTextColor(Color.parseColor(black));
                    signup_btu.setBackgroundResource(R.drawable.button_login_focus);
                    signup_btu.setTextColor(Color.parseColor(white));
                    phoneNoS.setError(null);
                    passwordS.setError(null);
                    email.setError(null);
                    name.setError(null);


                }
            }
        });

        login.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if(!isNetworkAvailable())
                        {
                            Toast.makeText(getApplicationContext(),"Network Error",Toast.LENGTH_SHORT).show();
                        }
                        else if(TextUtils.isEmpty(password.getText()) || TextUtils.isEmpty(phoneNo.getText()))
                        {
                            if(TextUtils.isEmpty(password.getText())) {

                                password.setError("Invalid Input");
                            }
                            if(TextUtils.isEmpty(phoneNo.getText()))
                            {
                                phoneNo.setError("Invalid Input");
                            }
                        }
                        else {
                            progressDialog.setTitle("");
                            progressDialog.setMessage("Please Wait");
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            BackgroundWorker bw = new BackgroundWorker(getApplicationContext());
                            bw.execute(phoneNo.getText().toString(), password.getText().toString(),"","");
                        }
                    }
                });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isNetworkAvailable())
                {
                    Toast.makeText(getApplicationContext(),"Network Error",Toast.LENGTH_SHORT).show();
                }
                else if(TextUtils.isEmpty(passwordS.getText()) || TextUtils.isEmpty(phoneNoS.getText())
                        || TextUtils.isEmpty(name.getText()) || TextUtils.isEmpty(email.getText()) ||
                        !Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches() )
                {
                    if(TextUtils.isEmpty(passwordS.getText())) {

                        passwordS.setError("Invalid Input");
                    }
                    if(TextUtils.isEmpty(phoneNoS.getText()))
                    {
                        phoneNoS.setError("Invalid Input");
                    }
                    if(TextUtils.isEmpty(email.getText())||!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches())
                    {
                        email.setError("Invalid Input");
                    }
                    if(TextUtils.isEmpty(name.getText()))
                    {
                        name.setError("Invalid Input");
                    }
                }
                else {
                    progressDialog.setTitle("");
                    progressDialog.setMessage("Please Wait");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    BackgroundWorker bw = new BackgroundWorker(getApplicationContext());
                    bw.execute(phoneNoS.getText().toString(), passwordS.getText().toString(),email.getText().toString(),name.getText().toString());
                }
            }
        });
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    class BackgroundWorker extends AsyncTask<String,String,String> {

        Context context;

        public BackgroundWorker(Context ctx){
            context=ctx;
        }
        @Override
        protected String doInBackground(String... params) {
            String phNo = params[0];
            String pass = params[1];
            String email = params[2];
            String name = params[3];
            String myurl= "https://ticket-otg.herokuapp.com/login";
            if(name.length() != 0)
             myurl= "https://ticket-otg.herokuapp.com/signup";

            String result = "";
            String line ="";
            try{
                URL url = new URL(myurl);
                HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String data = URLEncoder.encode("phone","UTF-8") + "=" + URLEncoder.encode(phNo,"UTF-8")+"&"
                        +URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(pass,"UTF-8");
                if(name.length() !=0);
                    data+= "&"+URLEncoder.encode("name","UTF-8")+"="+URLEncoder.encode(name,"UTF-8")+"&"+
                            URLEncoder.encode("email","UTF-8")+"="+URLEncoder.encode(email,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                while((line = bufferedReader.readLine())!= null)
                    result += line;
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result){

//            Toast.makeText(context,result,Toast.LENGTH_LONG).show();

            try {
                if(result.length() != 0)
                {


                    JSONObject jsonObject = new JSONObject(result);

                    String msg = jsonObject.getString("msg");

                    if(msg.length() !=0 )
                    {
                        progressDialog.cancel();
                        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
                    }
                    else {
                        JSONObject user = jsonObject.getJSONObject("user");
                        JSONArray history = user.getJSONArray("history");
                        Intent n = new Intent(getApplicationContext(), Welcome.class);
                        n.putExtra("token", user.getString("_id"));
                        n.putExtra("history", history.toString());
                        startActivity(n);
                        progressDialog.cancel();
                        Toast.makeText(context,"Login Sucessful",Toast.LENGTH_SHORT).show();

                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
    @Override
    protected void onStart(){
        super.onStart();
        password.setText("");
        phoneNo.setText("");
        passwordS.setText("");
        phoneNoS.setText("");
        name.setText("");
        email.setText("");
        myDefault();
    }
    public void myDefault()
    {
        toggle=true;
        login_page.setVisibility(View.VISIBLE);
        signup_page.setVisibility(View.INVISIBLE);


        login_btu.setBackgroundResource(R.drawable.button_login_focus);
        login_btu.setTextColor(Color.parseColor(white));
        signup_btu.setBackgroundResource(R.drawable.button_login_unfocus);
        signup_btu.setTextColor(Color.parseColor(black));
    }
}
