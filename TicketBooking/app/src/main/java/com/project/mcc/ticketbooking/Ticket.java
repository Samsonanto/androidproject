package com.project.mcc.ticketbooking;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

public class Ticket implements Serializable {
    protected String id;
    protected String src;
    protected String dst;
    protected String amt;
    protected String cst;
    protected String type;
    protected Date date;
    protected String time;
    public Ticket(String id, String src,String dst,String amt,String cst,String type,Date date,String time) {
        this.id = id;
        this.src = src;
        this.dst = dst;
        this.amt = amt;
        this.type = type;
        this.cst=cst;
        this.time =time;
        this.date = date;
    }



}
