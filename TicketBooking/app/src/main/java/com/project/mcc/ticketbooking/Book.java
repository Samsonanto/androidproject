package com.project.mcc.ticketbooking;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Book extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private String token;
    private Spinner sp1;
    private Spinner sp2;
    private EditText nos;
    private TextView cst;
    private CheckBox ret;
    private Ticket booked;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        Intent i =getIntent();
        if(i !=null)
            token = i.getStringExtra("token");

        if(token.length()==0){
            finish();
        }
        booked = null;

        nos = (EditText) findViewById(R.id.nos);
        cst = (TextView)findViewById(R.id.cst);
        ret = (CheckBox)findViewById(R.id.ret);
//        Spinner Init
        final String st[] = Station.getStation();
        sp1 = (Spinner)findViewById(R.id.sp1);
        sp2 = (Spinner)findViewById(R.id.sp2);
        CustomSpinnerAdapter customSpinnerAdapter=new CustomSpinnerAdapter(this,st);
        sp1.setAdapter(customSpinnerAdapter);
        sp2.setAdapter(customSpinnerAdapter);


        nos.addTextChangedListener(new TextWatcher() {


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String s1  = sp1.getSelectedItem().toString();
                String s2 = sp2.getSelectedItem().toString();

                if(!s1.equals(s2))
                {
                    int n;

                    if(!TextUtils.isEmpty(nos.getText())) {
                        n = Integer.parseInt(nos.getText().toString());
                        if(ret.isChecked())
                        {
                            cst.setText("₹ "+n*10);
                        }
                        else
                            cst.setText("₹ "+n*5);
                    }
                    else
                        cst.setText("Cost");

                }
            }
        });



        sp1.setOnItemSelectedListener(this);
        sp2.setOnItemSelectedListener(this);


        ret.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {

                String s1 = sp1.getSelectedItem().toString();
                String s2 = sp2.getSelectedItem().toString();


                if(!TextUtils.isEmpty(nos.getText()) && !s1.equals(s2)) {

                      if (ret.isChecked()) {
                          int n = Integer.parseInt(nos.getText().toString());
                          cst.setText("₹ "+n*10);
                      } else {
                          int n = Integer.parseInt(nos.getText().toString());
                          cst.setText("₹ "+n*5);
                    }
                }
            }
        });

//        sp1.getOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
//
//
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });



    }


    @Override
    public void onBackPressed(){
        if(booked != null) {
            Intent i = new Intent();
            i.putExtra("history", booked);
            setResult(RESULT_OK, i);
        }
        finish();


    }

    public void genQR(JSONObject history){

        try {
            String qr = history.getString("qrCode");
            FragmentManager fm = getFragmentManager();
            ViewQRCode p =new ViewQRCode();
            Bundle b= new Bundle();
            b.putString("qrCode",qr);
            b.putString("caller","Book");
            p.setArguments(b);
            p.show(fm,"QR Code");


            String time = history.getString("datePrc");

            time = time.substring(0,19);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); // your format
            Date d =format.parse(time);


            booked = new Ticket(history.getString("qrCode"),
                    history.getString("src"),
                    history.getString("dst"),
                    history.getString("qty"),
                    history.getString("cst"),
                    history.getString("typ"),
                    d,history.getString("time"));

        } catch (JSONException e) {
            Toast.makeText(getApplicationContext(),"JSON Exception",Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void payment(View view) {


        String s1  = sp1.getSelectedItem().toString();
        String s2 = sp2.getSelectedItem().toString();
        if(s1.equals(s2))
        {
            Toast.makeText(getApplicationContext(),"Change the Destination",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(nos.getText())){
            nos.setError("Invalid Input");
            return;
        }

        FragmentManager fm = getFragmentManager();
        Payment p =new Payment();
        Bundle b= new Bundle();
        b.putString("src",sp1.getSelectedItem().toString());
        b.putString("dst",sp2.getSelectedItem().toString());
        if(ret.isChecked())
            b.putString("ret","Return");
        else
            b.putString("ret","Single");
        b.putString("amt",nos.getText().toString());
        b.putString("totalCost",cst.getText().toString());
        b.putString("token",token);
        p.setArguments(b);
        p.show(fm,"Payment Window");
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String s1 = sp1.getSelectedItem().toString();
        String s2 = sp2.getSelectedItem().toString();

        if(s1.equals(s2)){
            cst.setText("Cost");
            return;
        }

        if(cst.getText().toString().equals("Cost") && !TextUtils.isEmpty(nos.getText())){

            int n = Integer.parseInt(nos.getText().toString());
            if(ret.isChecked())
            {
                cst.setText("₹ "+n*10);
            }
            else
                cst.setText("₹ "+n*5);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

        private final Context activity;
        private String[] station;

        public CustomSpinnerAdapter(Context context,String[] station) {
            this.station=station;
            activity = context;
        }



        public int getCount()
        {
            return station.length;
        }

        public Object getItem(int i)
        {
            return station[i];
        }

        public long getItemId(int i)
        {
            return (long)i;
        }



        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView txt = new TextView(getApplicationContext());
            txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(18);
            txt.setGravity(Gravity.CENTER_VERTICAL);
            txt.setText(station[position]);
            txt.setTextColor(Color.parseColor("#000000"));
            return  txt;
        }

        public View getView(int i, View view, ViewGroup viewgroup) {
            TextView txt = new TextView(getApplicationContext());
            txt.setGravity(Gravity.CENTER);
            txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(16);
            txt.setText(station[i]);
            txt.setTextColor(Color.parseColor("#000000"));
            return  txt;
        }

    }


}
