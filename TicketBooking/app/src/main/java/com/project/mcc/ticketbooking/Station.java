package com.project.mcc.ticketbooking;


public class Station {

    static final String[] station = {
            "CST",
            "Churchgate",
            "Bandra",
            "Vashi",
            "Andari",
            "Wadala",
            "CBD"
    };

    public static String[] getStation()
    {
        return station;
    }
}
