package com.project.mcc.ticketbooking;

import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class History extends AppCompatActivity {

    private RecyclerView r;
    private RecyclerView.Adapter tAdapter;
    private RecyclerView.LayoutManager lm;
    private String token;
    private int back ;
    private ArrayList<Ticket> ticketList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        Bundle fromWelcome = getIntent().getExtras();
        token = fromWelcome.getString("token");


        back=1;
        r = (RecyclerView)findViewById(R.id.historyRV);
        r.setHasFixedSize(true);
        lm = new LinearLayoutManager(this);
        r.setLayoutManager(lm);

        ConstructHistory c = (ConstructHistory) getIntent().getSerializableExtra("history");

        ticketList= c.getTicketList();

//       for(int i=0;i<10;i++)
//           ticketList.add(new Ticket("ID : "+i,"SOURCE"+i,"DESTINATION"+i,"QUANTITY :  QTY"+i,"COST : CST"+i,"TYPE"+i));

        tAdapter = new TicketAdaptor(ticketList);
        r.setAdapter(tAdapter);

    }


    public void CardClick(View view) throws ParseException {

        TextView tv= (TextView) view.findViewById(R.id.ticketId);
        String  s = tv.getText().toString();
        Date d = new Date();
        String tim;
        for(Ticket t : ticketList)
        {
            if(t.id.equals(s)){
                d = t.date;
                tim=t.time;
                break;
            }
        }
        String tmp;
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); // your format
        Date curDate = new Date();
        tmp = ft.format(new Date());
        try {
            curDate = ft.parse(tmp);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diff  = curDate.getTime() - d.getTime();
        long inHrs = diff /(1000 * 60 *60);

        if(inHrs > 4)
        {
            Toast.makeText(getApplicationContext(),"Ticket Expired",Toast.LENGTH_SHORT).show();
            return;
        }
        back=0;

        FragmentManager fm = getFragmentManager();
        Bundle b =new Bundle();
        b.putString("qrCode",s);
        b.putString("caller","History");
        ViewQRCode qr =new ViewQRCode();
        qr.setArguments(b);
        qr.show(fm,"QRCode");
    }




    public class TicketAdaptor extends
            RecyclerView.Adapter<TicketAdaptor.MyViewHolder> {

        private ArrayList<Ticket> ticketList;

        /**
         * View holder class
         * */
        public class MyViewHolder extends RecyclerView.ViewHolder{

            public TextView id;
            public TextView info;
            public TextView cst;
            public TextView amt;
            public TextView time;

            public MyViewHolder(View view) {
                super(view);
                id = (TextView) view.findViewById(R.id.ticketId);
                info = (TextView) view.findViewById(R.id.info);
                cst = (TextView) view.findViewById(R.id.cst);
                amt  = (TextView) view.findViewById(R.id.amt);
                time = (TextView)view.findViewById(R.id.time);

            }
        }

        public TicketAdaptor(ArrayList<Ticket> ticketList) {
            this.ticketList = ticketList;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            Ticket t = ticketList.get(position);
            holder.id.setText(t.id);
            holder.info.setText(t.src+" - "+t.dst+" ("+t.type+")");
            holder.cst.setText(t.cst);
            holder.amt.setText(t.amt);
            holder.time.setText(t.date.toString().substring(0,19));


        }

        @Override
        public int getItemCount() {
            return ticketList.size();
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listviewcard,parent, false);
            return new MyViewHolder(v);
        }
    }

}
