package com.project.mcc.ticketbooking;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ConstructHistory implements Serializable {

    ArrayList<Ticket> ticketList;




    ConstructHistory(String historyArray){

        ticketList = new ArrayList<Ticket>();

        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(historyArray);

            for(int i=0;i<jsonArray.length();i++)
            {
                JSONObject tmp  = jsonArray.getJSONObject(i);

                String s =  tmp.getString("datePrc");
                s = s.substring(0,19);

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); // your format
                Date d =format.parse(s);

                ticketList.add(0, new Ticket( tmp.getString("qrCode"), tmp.getString("src"),
                        tmp.getString("dst"), tmp.getString("qty") ,
                        tmp.getString("cst"), tmp.getString("typ"),
                        d , tmp.getString("time") ) );
            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    public ArrayList<Ticket> getTicketList() {
        return ticketList;
    }
    public void addTicket(Ticket t){
        ticketList.add(0,t);
    }
}
