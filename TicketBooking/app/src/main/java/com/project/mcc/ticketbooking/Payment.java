package com.project.mcc.ticketbooking;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class Payment  extends DialogFragment implements View.OnClickListener{
    View  paymentView;
    Button pay;
    RadioGroup tp;
    EditText cardName;
    EditText cardNo;
    EditText expire;
    EditText cvv;
    TextView cst;
    ProgressDialog progressDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        String Cost =  getArguments().getString("totalCost");
        progressDialog = new ProgressDialog(getActivity());
        paymentView = inflater.inflate(R.layout.payment_dialog,container,false);
        pay = (Button) paymentView.findViewById(R.id.pay);
        tp = (RadioGroup) paymentView.findViewById(R.id.tp);
        cardName = (EditText) paymentView.findViewById(R.id.cardName);
        cardNo = (EditText) paymentView.findViewById(R.id.cardNo);
        expire = (EditText) paymentView.findViewById(R.id.expire);
        cvv = (EditText) paymentView.findViewById(R.id.cvv);
        cst = (TextView)paymentView.findViewById(R.id.cst);
        cst.setText(Cost);
        tp.check(R.id.tp1);
        pay.setOnClickListener(this);
        return paymentView ;
    }


    public Boolean isEmpty(){

        if(TextUtils.isEmpty(cardName.getText()))
        {
            cardName.setError("Invalid Input");
        }
        if(TextUtils.isEmpty(cardNo.getText())){
            cardNo.setError("Invalid Input");
        }
        if(TextUtils.isEmpty(expire.getText()) ){
            expire.setError("Invalid Input");
        }
        if(TextUtils.isEmpty(cvv.getText())){
            cvv.setError("Invalid Input");
        }

        if(TextUtils.isEmpty(cardName.getText()) ||
                TextUtils.isEmpty(cardNo.getText()) ||
                TextUtils.isEmpty(expire.getText()) ||
                TextUtils.isEmpty(cvv.getText())
           ){
            return true;
        }

        return false;
    }


    @Override
    public void onClick(View v) {

        Boolean check =isEmpty() ;
        if(check) {
            return;
        }

        Context context = getActivity();
        Book b = (Book)getActivity();
        progressDialog.setTitle("");
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
        BackgroundWorker bw = new BackgroundWorker(context,b);
        String cst = getArguments().getString("totalCost").substring(2);
        String src = getArguments().getString("src");
        String dst = getArguments().getString("dst");
        String typ = getArguments().getString("ret");
        String amt = getArguments().getString("amt");

         bw.execute(src,dst,cst,amt,typ,getArguments().getString("token"));

         this.dismiss();

    }

    class BackgroundWorker extends AsyncTask<String,String,String> {

        Context context;
        Book b;
        public BackgroundWorker(Context ctx,Book b){
            context=ctx;
            this.b = b;
        }
        @Override
        protected String doInBackground(String... params) {
            String src = params[0];
            String dst = params[1];
            String cst = params[2];
            String amt = params[3];
            String type = params[4];
            String myurl = "https://ticket-otg.herokuapp.com/api/bookTicket?access_token="+params[5];
            String result = "";
            String line ="";
            try{
                URL url = new URL(myurl);
                HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                String data = URLEncoder.encode("src","UTF-8") + "=" + URLEncoder.encode(src,"UTF-8")+"&"
                        +URLEncoder.encode("dst","UTF-8")+"="+URLEncoder.encode(dst,"UTF-8")+
                        "&"+URLEncoder.encode("cst","UTF-8")+"="+URLEncoder.encode(cst,"UTF-8")+
                        "&"+URLEncoder.encode("amt","UTF-8")+"="+URLEncoder.encode(amt,"UTF-8")+
                        "&"+URLEncoder.encode("type","UTF-8")+"="+URLEncoder.encode(type,"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                while((line = bufferedReader.readLine())!= null)
                    result += line;
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result){

//            Toast.makeText(context,result,Toast.LENGTH_LONG).show();

            try {
                if(result.length() != 0) {


                    JSONObject jsonObject = new JSONObject(result);
//                    Toast.makeText(getActivity(),result,Toast.LENGTH_LONG).show();
//                    if(jsonObject.getString("ms"))

                    JSONObject history = jsonObject.getJSONObject("history");

                    b.genQR(history);
                    progressDialog.cancel();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

}
