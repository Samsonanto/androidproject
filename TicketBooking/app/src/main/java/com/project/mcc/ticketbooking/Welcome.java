package com.project.mcc.ticketbooking;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Welcome extends AppCompatActivity {

    private ConstructHistory constructHistory;
    private String token;
    private Intent fromLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        fromLogin= this.getIntent();
        if(fromLogin != null)
            token = fromLogin.getStringExtra("token");


            if(token.length() == 0)
            finish();

        constructHistory = new ConstructHistory(fromLogin.getStringExtra("history"));

        Button book = (Button) findViewById(R.id.book);
        book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n = new Intent(getApplicationContext(), Book.class);
                n.putExtra("token",token);
                startActivityForResult(n,1);
            }
        });
        Button history = (Button) findViewById(R.id.history);
        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n = new Intent(getApplicationContext(), History.class);
                n.putExtra("history",constructHistory);
                n.putExtra("token",token);
                startActivity(n);
            }
        });
        Button about = (Button) findViewById(R.id.about);
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n = new Intent(getApplicationContext(), About.class);
                startActivity(n);
            }
        });
        Button logout = (Button) findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        if(requestCode == 1 && resultCode == RESULT_OK){
            Ticket t = (Ticket) data.getSerializableExtra("history");
            constructHistory.addTicket(t);
            Toast.makeText(getApplicationContext(),"History added",Toast.LENGTH_SHORT).show();
        }
    }
}
